# deploy-gke

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag need to be enabled to use the Component.

`deploy-gke` is a component to deploy a container to a GKE cluster. It also performs horizontal pod auto-scaling up to 3 nodes and creates a `Service` if the application needs a port exposed.

## Prerequisites
- Google Cloud workload identity federation must be set up with [GitLab - Google Cloud integration onboarding process][onboarding].
- The workload identity used to create the release must have proper permissions configured. Please check [Authorization](#authorization) section.
- The image must be accessible by the GKE cluster.
- A Google Cloud project with a GKE cluster.

## Billing

Usage of the `deploy-gke` GitLab component might incur Google Cloud billing charges depending on your usage. For more information on GKE pricing, see [GKE Pricing][pricing].

## Usage

Include the deploy-gke component in your .gitlab-ci.yml file:

```yaml
include:
  - component: gitlab.com/google-gitlab-components/gke/deploy-gke@<VERSION>
    inputs:
      stage: build
      image: nginx
      app_name: nginx-app 
      cluster_name: my-cluster
      project_id: my-project
      region: us-central1
      expose_port: "8080"
```

## Authentication

- The component authenticates to Google Cloud services by workload identity Federation. Please follow [Gitlab - Google Cloud integration onboarding process][onboarding] to complete the workload identity Federation setup.

> - Requires `google_cloud_support_feature_flag` (Beta) flag need to be enabled.

## Inputs
| Input | Description | Example | Default Value |
| ----- | ----------- | ------- | ------------- |
| `as` | (Optional) The name of the job to be executed | `my-job` | `deploy-to-gke` |
| `stage` |(Optional): The GitLab CI/CD stage where the component will be executed | `my-stage` | `deploy` |
| `image` | Image to Deploy | `docker.io/nginx`| |
| `app_name` | Desired name of the application | `my-app` | | 
| `cluster_name` | Name of the GKE cluster | `my-cluster-name` | |
| `project_id` | GCP ProjectID | `my-gcp-project-id` | |
| `region` | Region of the Cluster | `us-central1`| |
| `namespace` | Namespace on GKE where to deploy the application | `my-namespace` | `"default"` |
| `expose_port` | Must match the port that your application is listening on. | `"8080"` | `"0"` |

## Authorization
To use the component, the provided workload identity must have the following minimum roles:
- Kubernetes Engine Developer ([`roles/container.developer`][sa-container-dev])
  - To provide access to Kubernetes API objects inside clusters
- Kubernetes Engine Cluster Viewer ([`roles/container.clusterViewer`][sa-cluster-viewer])
  - To provide access to get and list GKE clusters.
- Service Account User ([`roles/iam.serviceAccountUser`][sa-sa-user])
  - To run operations as the service account

For example, to grant the `roles/container.developer` role to all principals in a workload identity pool matching `developer_access=true` attribute mapping by the gcloud CLI:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/container.developer"
```

[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[pricing]: https://cloud.google.com/kubernetes-engine/pricing
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[sa-container-dev]: https://cloud.google.com/iam/docs/understanding-roles#container.developer
[sa-sa-user]: https://cloud.google.com/iam/docs/understanding-roles#iam.serviceAccountUser
[sa-cluster-viewer]: https://cloud.google.com/iam/docs/understanding-roles#container.clusterViewer


